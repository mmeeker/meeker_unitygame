﻿using UnityEngine;
using System.Collections.Generic;

public class AllyShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;


    public List<GameObject> EnemiesInRange = new List<GameObject>();

    bool inRange = false;
    float timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;

    GameController gameController;
  
    void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable");
        gunParticles = GetComponent<ParticleSystem>();
        gunLine = GetComponent<LineRenderer>();
        gunAudio = GetComponent<AudioSource>();
        gunLight = GetComponent<Light>();

        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        
        InvokeRepeating("CheckRange", .25F, .25F);
    }


    void Update()
    {
        timer += Time.deltaTime;
      
        if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && inRange)
        {
          
            Shoot();
        }

        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects();
        }
    }

    void CheckRange()
    {
        inRange = IsEnemyInRange();
    }

    bool IsEnemyInRange()
    {
        for (int i = 0; i < gameController.enemiesPool.Count; i++)
        {
            float distance = (transform.position - gameController.enemiesPool[i].transform.position).magnitude;
            if (distance < 10f)
            {
                return true;
            }
        }
        return false;
    }


    public void DisableEffects()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot()
    {
        timer = 0f;

        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();

        gunLine.enabled = true;
        gunLine.SetPosition(0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(damagePerShot, shootHit.point);
            }
            gunLine.SetPosition(1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
        }
    }    
}