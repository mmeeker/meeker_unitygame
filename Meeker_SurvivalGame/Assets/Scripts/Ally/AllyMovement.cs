﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyMovement : MonoBehaviour
{
    bool shooting = false;
    
    GameObject ClosestEnemy;
    GameObject player;   
    UnityEngine.AI.NavMeshAgent nav;

    public bool awaken = false;
    public GameObject particlePrefab;

    GameController gameController;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        InvokeRepeating("CheckClosest", .25F, .25F);
    }

    void Update()
    {
        shooting = Input.GetButton("Fire1");
        //the ally can only do 1 thing either fight or follow
        if (awaken)
        {
            if (shooting)
            {
                nav.enabled = false;
                //print(this.transform);
                transform.LookAt(ClosestEnemy.transform);
                //this.transform.LookAt(ClosestEnemy.transform.position);
            }
            else
                nav.enabled = true;
            if (nav.enabled)
                nav.SetDestination(player.transform.position);          
        }
        else
            nav.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")  && !awaken)
        {
            awaken = true;
            nav.enabled = true;
            Instantiate(particlePrefab, transform.position, particlePrefab.transform.rotation);
        }
    }

    void CheckClosest()
    {
        ClosestEnemy = ClosestEnemyInRange();
    }

    GameObject ClosestEnemyInRange()
    {
        //i don't care who the closest enemy is if i am not shooting
        if (shooting && gameController.enemiesPool.Count > 0)
        {
            GameObject closest = player;
            float shortestDistance = 100f;
            for (int i = 0; i < gameController.enemiesPool.Count; i++)
            {
                float distance = (transform.position - gameController.enemiesPool[i].transform.position).magnitude;

                if (distance < 10f)
                {
                    if (distance < shortestDistance)
                    {
                        shortestDistance = distance;
                        closest = gameController.enemiesPool[i];
                    }
                }                          
            }
            return closest;
        }
        else
            return player;
        
    }

}
