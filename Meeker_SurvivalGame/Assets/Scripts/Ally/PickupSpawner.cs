﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour       
{

    public GameObject pickupPrefab;
    GameObject pickupSpawned;
    float timer;

    // Start is called before the first frame update
    void Start()
    {
        SpawnPickup();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        
        if (timer >= 60f)
        {
            
            SpawnPickup();
            timer = 0f;

        }
    
    }

    void SpawnPickup()
    {
        pickupSpawned = Instantiate(pickupPrefab, transform.position, pickupPrefab.transform.rotation) as GameObject;
        
    }

}
