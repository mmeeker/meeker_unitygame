﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public List<GameObject> enemiesPool;


    // Start is called before the first frame update
    void Start()
    {
        enemiesPool = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddEnemy(GameObject toAdd)
    {
        enemiesPool.Add(toAdd);
    }

    public void RemoveEnemy(GameObject toRemove)
    {
        for (int i = 0; i < enemiesPool.Count; i++)
        {
            if (enemiesPool[i] == toRemove)
            {
                //print("Removed!");
                enemiesPool.RemoveAt(i);
                return;
            }
        }
    }
}
