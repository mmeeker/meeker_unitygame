﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particleDestroyer : MonoBehaviour
{

    public float timeToDestroy;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyMe", timeToDestroy);   
    }

    // Update is called once per frame
    void Update()
    {
           
    }

    void DestroyMe() {
        Destroy(gameObject);
    }

}
